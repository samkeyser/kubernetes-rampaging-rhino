import psycopg2
from minio import Minio
from minio.error import S3Error
# using flask_restful
from flask import Flask, jsonify, request
from flask_restful import Resource, Api
import json
from datetime import datetime
import os

def get_secret(secret):
    s = os.getenv('secret')
    if not s:
        with open(os.getenv((secret+"_FILE")), "r") as f:
            s = f.readline().strip()

    return s

def get_db_connection():
    conn = psycopg2.connect(
        database = os.getenv('POSTGRES_DATABASE'),
        user = get_secret('POSTGRES_USER'),
        password = get_secret('POSTGRES_PASSWORD'),
        host = os.getenv('POSTGRES_HOST'),
        port = os.getenv('POSTGRES_PORT'))

    conn.set_client_encoding('UTF8')

    return conn

# creating the flask app
app = Flask(__name__)
# creating an API object
api = Api(app)

# Class that represents the /rate object
class Rate(Resource):

    # Corresponds to POST request
    def post(self):
        result = -1
        data = request.get_json()
        valid_id = lambda prop: prop in data and int(data[prop]) > 0
        if valid_id('user_id') and valid_id('song_id') and 'rating' in data and int(data['rating']) in range(1,6):
            with get_db_connection() as conn:
                with conn.cursor() as cur:
                    timestamp = datetime.now()
                    cur.execute('INSERT INTO working (timestamp, user_id, song_id, rating) '
                            'VALUES (%s, %s, %s, %s) RETURNING user_id;',
                            [str(timestamp), str(data['user_id']), str(data['song_id']), str(data['rating'])])
                    result = cur.fetchone()[0]

        return {'user_id': result}, 201

# Adding the defined resources along with their corresponding urls
api.add_resource(Rate, '/rate')

# driver function
if __name__ == '__main__':
    # Have to add to 0.0.0.0 so we can hit it outside the container
    # https://devops.stackexchange.com/questions/3380/dockerized-flask-connection-reset-by-peer
    app.run(debug = False, host='0.0.0.0', port = 8888 )
