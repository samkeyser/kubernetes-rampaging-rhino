# I'm a pickle morty! pickle riiiiiiiiiiiiiiick
import os
import pickle
from minio import Minio
from time import perf_counter
class MinioAPI:
    def __init__(self):
        print(os.environ)
        minio_user = None
        with open(os.environ["MINIO_ROOT_USER_FILE"], 'r') as datafile:
            minio_user = datafile.readline().strip()

        minio_password = None
        with open(os.environ["MINIO_ROOT_PASSWORD_FILE"], 'r') as datafile:
            minio_password = datafile.readline().strip()


        print(os.environ)
        minio_host = os.environ["MINIO_URL"] + ":" + os.environ.get("MINIO_PORT")
        self.client = Minio(minio_host,
                            minio_user,
                            minio_password,
                            secure=False)

    def fput(self, bucket, object_name, filepath):
        if not self.client.bucket_exists(bucket):
            self.client.make_bucket(bucket)

        self.client.fput_object(bucket, object_name, filepath)

    def fget(self, bucket, object_name, destination):
        assert self.client.bucket_exists(bucket), f"{bucket} does not exist"

        self.client.fget(bucket, object_name, destination)

def main():
    minio = MinioAPI()
    # Pull down the pickles only if the bucket is in minio (implies that the model objects also exist)
    if minio.client.bucket_exists("user-item-data"):
        minio.fget("user-item-data", "svd_model.pickle")
        minio.fget("user-item-data", "avg_rating_model.pickle")
    pass

if __name__ == "__main__":
    main()
