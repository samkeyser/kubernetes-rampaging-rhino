# Import modules
import pickle
import numpy as np
from tqdm.notebook import tqdm
from scipy.stats import pearsonr, tmean
from sklearn.decomposition import TruncatedSVD
from scipy.sparse import csr_matrix, csc_matrix

class SVDModel:
    def __init__(self, n_components=None, model=None):
        if model:
            self.SVD = model
            self.k = self.SVD.components_.shape[0]
        elif n_components:
            self.k = n_components
            self.SVD = TruncatedSVD(n_components=self.k)
        else:
            raise Exception("Must provide one or the other: model or n_components")

    def from_pickle(pickle_file):
        with open(pickle_file, 'rb') as datafile:
            svd = pickle.load(datafile)
        return SVDModel(model=svd)

    def to_pickle(self, fname='svd_model.pickle'):
        with open(fname, 'wb') as datafile:
            pickle.dump(self.SVD, datafile)
        return fname

    def fit(self, X):
        self.SVD.fit(X)

    def predict(self, user_ratings):
        reduced_user_ratings = self.SVD.transform(user_ratings)
        recast = self.SVD.inverse_transform(reduced_user_ratings)
        return csr_matrix(recast)

class AverageRatingModel:
    def __init__(self, incl_zeros=False, avg=None):
        self.incl_zeros = incl_zeros
        if avg is not None:
            self.avg = avg

    def from_pickle(pickle_file):
        with open(pickle_file, 'rb') as datafile:
            (incl_zeros, avg) = pickle.load(datafile)
        return AverageRatingModel(incl_zeros=incl_zeros, avg=avg)

    def to_pickle(self, fname='avg_rating_model.pickle'):
        with open(fname, 'wb') as datafile:
            pickle.dump((self.incl_zeros, self.avg), datafile)
        return fname

    def _tile(self, vec, n, dtype):
        return csr_matrix(np.ones([n,1],dtype=dtype) *\
            csr_matrix(self.avg, dtype=dtype))

    def fit(self, training_ratings):
        self.X = training_ratings

        self.avg = self.X.sum(axis=0)
        if self.incl_zeros:
            self.avg = self.avg / self.X.shape[0]
        else:
            col_optimized = csc_matrix(self.X)
            denom = (col_optimized > 0).sum(axis=0)
            denom[denom == 0] = 1
            self.avg = self.avg / denom
        self.avg = np.asarray(self.avg)

    def predict(self, user_ratings, dtype=np.float32, condense=False):
        if condense:
            return csr_matrix(self.avg, dtype=dtype)
        else:
            return self._tile(self.avg, user_ratings.shape[0], dtype)
