import pickle
import numpy as np
from tqdm import tqdm
from models import SVDModel
from scipy.stats import pearsonr
from data import get_data, push_data
from sklearn.model_selection import train_test_split

def pcc(predicted, true):
    r, _ = pearsonr(predicted, true)
    return r

def evaluate_model(model, X_test, batch_size=1024):
    predicted, true = [], []
    for i in tqdm(range(0,round(X_test.shape[0]/batch_size))):
        batch = X_test[i*batch_size:(i*batch_size)+batch_size]
        pred = model.predict(batch)
        mask = batch > 0

        predicted += list(np.asarray(pred[mask]).ravel())
        true += list(np.asarray(batch[mask]).ravel())
    if len(predicted) > 2 and len(true) > 2:
        r2 = pcc(predicted, true)
    else:
        r2 = 0
    return r2

def svd_elbow(X_train, X_test, min_delta=0.00001):
    n = 25
    r2 = -1
    prev_r2 = r2-min_delta
    optimal_n = n
    while (r2-prev_r2) > min_delta:
        optimal_n = n
        # TODO: I'd like to have a more sophisticated way for increasing this
        n += 100
        # Initialize model
        model = SVDModel(n)
        model.fit(X_train)

        # Evalute model
        prev_r2 = r2
        r2 = evaluate_model(model, X_test)
    return optimal_n

def main():
    # Grab the data from minio
    training_data = get_data()
    print("training_data.shape =", training_data.shape)

    # Train / test split
    X_train, X_test = train_test_split(training_data, test_size=0.2)
    print("X_train.shape =", X_train.shape)
    print("X_test.shape =", X_test.shape)

    # Compute elbow
    optimal_n = 125#svd_elbow(X_train, X_test)
    print("Chose n =", optimal_n)

    # Initialize model
    model = SVDModel(n_components=optimal_n)

    # Train model
    model.fit(training_data)

    # Dump model to minio
    pickle_file = model.to_pickle()
    push_data(pickle_file)

if __name__ == "__main__":
    main()
