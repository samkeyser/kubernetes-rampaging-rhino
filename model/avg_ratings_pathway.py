import pickle

from data import get_data, push_data
from models import AverageRatingModel

def main():
    # Grab the data from minio
    training_data = get_data()

    # Initialize model
    model = AverageRatingModel()

    # Train model
    model.fit(training_data)

    # Dump model to minio
    pickle_file = model.to_pickle()
    push_data(pickle_file)

if __name__ == "__main__":
    main()
