import os
import json
import pickle
import psycopg2
from minio import Minio
from minio.error import S3Error
from time import perf_counter
from scipy.sparse import csr_matrix

def get_secret(secret):
    s = os.getenv('secret')
    if not s:
        with open(os.getenv((secret+"_FILE")), "r") as f:
            s = f.readline().strip()

    return s

def get_db_connection():
    conn = psycopg2.connect(
        database = os.getenv('POSTGRES_DATABASE'),
        user = get_secret('POSTGRES_USER'),
        password = get_secret('POSTGRES_PASSWORD'),
        host = os.getenv('POSTGRES_HOST'),
        port = os.getenv('POSTGRES_PORT'))

    conn.set_client_encoding('UTF8')

    return conn

class MinioAPI:
    def __init__(self):
        minio_user = None
        with open(os.environ.get("MINIO_ROOT_USER_FILE"), 'r') as datafile:
            minio_user = datafile.readline().strip()

        minio_password = None
        with open(os.environ.get("MINIO_ROOT_PASSWORD_FILE"), 'r') as datafile:
            minio_password = datafile.readline().strip()


        minio_host = os.environ.get("MINIO_URL") + ":" + os.environ.get("MINIO_PORT")
        self.client = Minio(minio_host,
                            minio_user,
                            minio_password,
                            secure=False)

    def fput(self, bucket, object_name, filepath):
        if not self.client.bucket_exists(bucket):
            self.client.make_bucket(bucket)

        self.client.fput_object(bucket, object_name, filepath)

    def fget(self, bucket, object_name, destination):
        assert self.client.bucket_exists(bucket), f"{bucket} does not exist"

        self.client.fget_object(bucket, object_name, destination)

def main():
    # TODO: these should be envars
    SPARSE_MAT_OBJ = os.environ.get("SPARSE_MAT_OBJ", "sparse_matrix.pickle")
    BATCH_SIZE = 16384
    MAX_ROWS = int(os.environ.get("MAX_ROWS", 10000))

    # TODO: need to move all data from working into ratings and then clear out working

    rows = []
    cols = []
    values = []
    print("Initializing database connection", flush=True)
    fetch_time_start = perf_counter()
    with get_db_connection() as conn:
        print("\tInitializing cursor", flush=True)
        with conn.cursor() as cur:
            print("\t\tMoving working table entries to ratings table", flush=True)
            cur.execute("WITH selection AS ( DELETE FROM working RETURNING * ) INSERT INTO ratings SELECT * FROM selection;")
        print("\tDone moving rows", flush=True)

        print("\tInitializing cursor", flush=True)
        with conn.cursor('server-side-data-processing-cursor') as cur:
            # First, we need to groupby user_id, song_id and get ratings
            ## Execute the query. Use group by to implicitly sort
            ### Since this is a MASSIVE query, we need to do some batching
            ### https://stackoverflow.com/questions/28343240/psycopg2-uses-up-memory-on-large-select-query
            ### https://www.psycopg.org/docs/usage.html#server-side-cursors
            cur.itersize = BATCH_SIZE
            cur.execute(f'SELECT user_id, song_id, rating FROM ratings GROUP BY user_id, song_id, rating LIMIT {MAX_ROWS};')
            print("\tExecuted cursor query", flush=True)

            ## Read the data out from the query in batches
            print("\tReading batches", flush=True)
            num_batches = 0
            batch = cur.fetchmany(BATCH_SIZE)
            while len(batch) > 0:
                num_batches += 1
                for user, song, rating in batch:
                    rows.append(int(user))
                    cols.append(int(song))
                    values.append(int(rating))
                batch = cur.fetchmany(BATCH_SIZE)
    with conn.cursor('server-side-recommend-endpoint-cursor-query-items') as cur:
        cur.execute('SELECT DISTINCT MAX(song_id) from ratings;')
        col_items = int(cur.fetchone()[0])+1
    with conn.cursor('server-side-recommend-endpoint-cursor-query-items') as cur:
        cur.execute('SELECT DISTINCT MAX(user_id) from ratings;')
        row_items = int(cur.fetchone()[0])+1
    fetch_time_stop = perf_counter()
    print('Pulled', len(rows), 'rows in', (fetch_time_stop-fetch_time_start), 'seconds!', flush=True)

    # Then we need to make the sparse matrix and pickle it
    print('Creating sparse matrix from values', flush=True)
    sparse_mat = csr_matrix((values, (rows, cols)), shape=(row_items,col_items))

    print('Pickling sparse matrix', flush=True)
    with open(SPARSE_MAT_OBJ, 'wb') as datafile:
        pickle.dump(sparse_mat, datafile)

    ## Finally we need to yeet the pickle into minio
    print('Creating minio api object', flush=True)
    minio = MinioAPI()
    print('Uploading sparse matrix to minio', flush=True)
    minio.fput("model-data", SPARSE_MAT_OBJ, SPARSE_MAT_OBJ)

    print('Done', flush=True)

if __name__ == "__main__":
    main()
