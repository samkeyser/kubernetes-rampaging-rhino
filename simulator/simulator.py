import argparse
import datetime as dt
import glob
import json
import math
import numpy as np
import os
import random
import sys

import requests

from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score


def run_user_simulation(args):
    url = args.rate_url+'/rate'
    path = os.path.join(args.rating_dir, args.rating_file+"_*.txt")

    files = glob.glob(path)

    for file in files:
        with open(file) as fl:
            for l in fl:
                user_id, song_id, rating = l.strip().split('\t')
                record = {"user_id": user_id, "song_id": song_id, "rating": rating}
                response = requests.post(url, json=record)
                if not response.ok:
                    print("Error: {} {}".format(response.status_code, response.reason), file=sys.stderr)
                    sys.exit(1)

def run_evaluation(args):
    url = args.recommend_url+'/recommend'
    path = os.path.join(args.rating_dir, args.rating_file+"_*.txt")

    files = glob.glob(path)

    true_labels = {}
    recall = {}

    for file in files:
        with open(file) as fl:
            for l in fl:
                user_id, song_id, rating = l.strip().split('\t')

                tmp = true_labels.get(user_id, set())
                tmp.add(song_id)

                true_labels[user_id] = tmp

    total_recommendations = args.total_recommendations if args.total_recommendations else 10

    for u in true_labels.keys():
        record = {"user_id": u, "total_recommendations": total_recommendations}
        response = requests.get(url, json=record)

        if not response.ok:
            print("Error: {} {}".format(response.status_code, response.reason), file=sys.stderr)
            sys.exit(1)

        json_response = response.json()

#        print("Top 5 recommendations for user ", u, ": ",  json_response['recommendations'][:5])


        for i in range(0, total_recommendations, 10):
            tmp = recall.get(i, [])

            remaining = len(json_response['recommendations'][i:])

            x = i+10
            if remaining < 10:
                x = remaining

            p1 = len(true_labels[u].intersection(set(json_response['recommendations'][i:x]))) / x

            p2 = 0
            if i != 0:
                p2 = sum(recall[i-10])

            tmp.append(p1+p2)
            recall[i] = tmp


    for r in recall.keys():
        print("Recall for set size ", 10+r, ": ", sum(recall[r])/len(recall[r]))


def parse_args():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="mode", required=True)

    eval_parser = subparsers.add_parser("evaluate-model")

    eval_parser.add_argument("--rating-dir",
                             type=str,
                             required=True)

    eval_parser.add_argument("--rating-file",
                             type=str,
                             required=True)

    eval_parser.add_argument("--rate-url",
                             type=str,
                             required=True)

    eval_parser.add_argument("--recommend-url",
                             type=str,
                             required=True)

    eval_parser.add_argument("--total-recommendations",
                             type=int,
                             required=False)

    user_parser = subparsers.add_parser("simulate-user")

    user_parser.add_argument("--rating-dir",
                             type=str,
                             required=True)

    user_parser.add_argument("--rate-url",
                             type=str,
                             required=True)

    user_parser.add_argument("--rating-file",
                             type=str,
                             required=True)

    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()

    if args.mode == "evaluate-model":
        run_evaluation(args)
    elif args.mode == "simulate-user":
        run_user_simulation(args)
